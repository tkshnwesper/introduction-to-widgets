import styled from "styled-components";

export const HalfWidth = styled.div`
width: 50vw
`

export const FlutterBlue = styled.span`
color: #5ec9f8
`

export const StatelessGreen = styled.span`
color: #8bc34a
`

export const StatefulBlue = styled.span`
color: #039be5
`

export const YellowGreen = styled.span`
color: yellowgreen
`

export const SlateBlue = styled.span`
color: slateblue
`

export const Orange = styled.span`
color: orange
`

export const HotPink = styled.span`
color: hotpink
`

export const Yellow = styled.span`
color: yellow
`

export const Fuchsia = styled.span`
color: fuchsia
`

export const LimeGreen = styled.span`
color: limegreen
`

export const MediumTurquoise = styled.span`
color: mediumturquoise
`

export const PaleGoldenRod = styled.span`
color: palegoldenrod
`