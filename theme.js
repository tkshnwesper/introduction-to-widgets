import { notes as theme, dark } from '@mdx-deck/themes'
import highlight from '@mdx-deck/themes/syntax-highlighter-prism'
import { atomDark as style } from 'react-syntax-highlighter/dist/styles/prism'

export default [
  highlight({ prism: { style } }),
  {
    ...theme,

    colors: {
      ...dark.colors,
    },

    pre: {
      fontSize: '0.6em'
    },

    ul: {
      lineHeight: '1.7em',
    },
    p: {
      lineHeight: '1.7em'
    }
  }
]
