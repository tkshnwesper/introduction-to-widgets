import { slides as _00 } from "./slides/00_Intro_to_Flutter.mdx"
import { slides as _01 } from "./slides/01_Stateful_and_Stateless.mdx"
import { slides as _02 } from "./slides/02_Routing.mdx";

export const slides = [..._00, ..._01, ..._02]

export { default as themes } from './theme'
